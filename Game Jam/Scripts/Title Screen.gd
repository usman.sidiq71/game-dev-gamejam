extends Node2D

export(String) var scene_to_load

func _ready():
	$"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/New Game".grab_focus()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _physics_process(delta):
	if $"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/New Game".is_hovered() == true:
		$"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/New Game".grab_focus()
	if $"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Continue".is_hovered() == true:
		$"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Continue".grab_focus()
	if $"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Exit".is_hovered() == true:
		$"MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Exit".grab_focus()

func _on_New_Game_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
