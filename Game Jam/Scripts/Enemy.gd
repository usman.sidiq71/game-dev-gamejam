extends KinematicBody2D

const GRAVITY = 10
const SPEED = 50
const FLOOR = Vector2(0, -1)

export (String) var sceneName = "Level 1"

var velocity = Vector2()

var direction = -1
var is_dead = false

func dead():
	is_dead = true
	velocity = Vector2(0, 0)
	$AnimatedSprite.play("Dead")
	global.coins += 1
	$CollisionShape2D.disabled = true
	$Timer.start()

func _physics_process(delta):
	if is_dead == false:
		velocity.x = SPEED * direction
	
		if direction == 1:
			$AnimatedSprite.flip_h = true
		else:
			$AnimatedSprite.flip_h = false
		$AnimatedSprite.play("Walk")
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, FLOOR)
	
		if is_on_wall() or $RayCast2D.is_colliding() == false:
			direction = direction * -1
			$RayCast2D.position.x *= -1
			$RayCast2D2.position.x *= -1
	
		if $RayCast2D2.is_colliding() and $RayCast2D2.get_collider() == get_parent().get_parent().get_node("Player"):
			if $RayCast2D2.get_collider():
				global.lives -=1
				global.dead_count += 1
				global.coins = 0
				if (global.lives <= 0):
					get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
				else:
					get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
		
		if $RayCast2D3.is_colliding() and $RayCast2D3.get_collider() == get_parent().get_parent().get_node("Player"):
			dead()

func _on_Timer_timeout():
	queue_free()
