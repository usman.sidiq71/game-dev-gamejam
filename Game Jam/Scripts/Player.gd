extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -500
export (int) var GRAVITY = 1200

onready var sprite = get_node("Sprite")

const FIREBALL = preload("res://Scenes/Projectile.tscn")
const UP = Vector2(0, -1)
var velocity = Vector2()

var on_ground = false
var jump_count = 0


func get_input():
	velocity.x = 0
	var direction = 0
	if Input.is_action_just_pressed('ui_up'):
			if jump_count < 1:
				if global.dead_count == 0:
					if on_ground == true:
						velocity.y = jump_speed
				else:
					velocity.y = jump_speed
					on_ground = false
					jump_count = jump_count + 1
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		direction = 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		direction = -1
	
	if is_on_floor():
		on_ground = true
		jump_count = 0
	else:
		on_ground = false
	
	if	velocity.x == 0:
		if on_ground == false:
			sprite.play("Jump")
		else:
			sprite.play("Idle")
	else:
		if on_ground == false:
			sprite.play("Jump")
		else:
			if Input.is_action_pressed('ui_select') and is_on_floor():
				speed = 600
				sprite.play("Run")
			else:
				speed = 400
				sprite.play("Walk")
	if velocity.x > 0:
		sprite.set_flip_h(false)
	elif velocity.x < 0:
		sprite.set_flip_h(true)	
		
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	
	var snap = Vector2.DOWN * 32 if !on_ground else Vector2.ZERO
	velocity = move_and_slide_with_snap(velocity, snap, UP)

