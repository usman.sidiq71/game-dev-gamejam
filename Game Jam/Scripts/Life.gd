extends Label

func _process(delta):
	self.text = "x " + str(global.lives)
	
	if global.coins > 10:
		get_tree().reload_current_scene()
		global.coins = 0
		global.dead_count += 1
		global.lives -= 1