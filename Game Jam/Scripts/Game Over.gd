extends LinkButton

func _on_Back_to_Main_Menu_pressed():
	global.lives = 3
	global.dead_count = 0
	global.coins = 0
	get_tree().change_scene("res://Scenes/Title Screen.tscn")
