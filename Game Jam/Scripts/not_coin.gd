extends Area2D

export (String) var sceneName = "Level 1"

func _on_not_coin_0_body_entered(body):
	global.coins = 0
	global.lives -= 1
	global.dead_count += 1
	if (global.lives <= 0):
		get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
	else:
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
